# Aeropress Dice

A web based version of [James Hoffmann's Aeropress Dice](https://web.archive.org/web/20171208104650/https://jimseven.com/2017/12/06/coffee-brewing-dice/).

After creating this I discovered that there is a [similar project created by thewinniewu](https://github.com/thewinniewu/aeropress-dice).
