const COFFEE_WATER_RADIO = [
    {"coffee": 12, "water": 200},
    {"coffee": 15, "water": 200},
    {"coffee": 15, "water": 250},
    {"coffee": 24, "water": 200, "note": "Dilute to share"},
    {"coffee": 30, "water": 200, "note": "Dilute to share"},
    {"your_choice": true}
];

const TEMP = [
    {"cel": 95},
    {"cel": 90},
    {"cel": 85},
    {"cel": 80},
    {"cel": 75},
    {"your_choice": true}
];

const GRIND_BREW_TIME = [
    {"grind": "very fine", "time":  30},
    {"grind": "fine", "time":  60},
    {"grind": "medium fine", "time":  90},
    {"grind": "medium", "time":  120},
    {"grind": "coarse", "time": 240},
    {"your_choice": true}
];

const METHOD = [
    {"version": "standard", "bloom": {"time":  0, "water":  0}},
    {"version": "standard", "bloom": {"time": 30, "water": 30}},
    {"version": "standard", "bloom": {"time": 30, "water": 60}},
    {"version": "inverted", "bloom": {"time":  0, "water":  0}},
    {"version": "inverted", "bloom": {"time": 30, "water": 30}},
    {"version": "inverted", "bloom": {"time": 30, "water": 60}}
];

const AGITATION = [
    {"text": "Stir once before pressing", "img": "#"},  // round 1
    {"text": "Don't stir", "img": "#"},
    {"text": "Stir twice before pressing", "img": "#"}, // round x2
    {"text": "Stir in a cross shape before pressing", "img": "#"}, // compass
    {"text": "Stir in both directions before pressing", "img": "#"}, // both directions
    {"your_choice": true}
];


function celsius_to_fahrenheit(temp_cel) {
    let temp_fah = temp_cel * 1.8 + 32;
    return temp_fah;
}

function random_item(arr) {
    let i = Math.floor(Math.random()*arr.length);
    return [i, arr[i]];
}

function generate_coffee_water_html(obj) {
    if (obj.your_choice == true) {
        return "<span>Your Choice</span>";
    } else {
        let html = "<span>"+obj.coffee+"g</span><span>"+obj.water+"g</span>";
        if (obj.note) {
            html += "<i>(" + obj.note + ")</i>";
        }
        return html;
    }
}

function generate_temp_html(obj) {
    if (obj.your_choice == true) {
        return "<span>Your Choice</span>";
    } else {
        let fah = celsius_to_fahrenheit(obj.cel);
        return "<span>"+obj.cel+"&deg;C</span><span>"+fah+"&deg;F</span>";
    }
}

function generate_gbt_html(obj) {
    if (obj.your_choice == true) {
        return "<span>Your Choice</span>";
    } else {
        let min = Math.floor(obj.time / 60);
        if (min == 0) {
            min = "";
        } else {
            min += " min ";
        }
        let sec = obj.time % 60;
        if (sec == 0) {
            sec = "";
        } else {
            sec += " sec";
        }
        return "<span>"+obj.grind+"</span><span>"+min+sec+"</span>";
    }
}

function generate_method_html(obj) {
    let html = "<span>"+obj.version+"</span>";
    if (obj.bloom.time == 0) {
        return html + "<span>No Bloom</span>";
    }
    return html + "<span>"+obj.bloom.time+"s bloom</span><span>"+obj.bloom.water+"g water</span>";
}

function generate_agitation_html(obj) {
    if (obj.your_choice == true) {
        return "<span>Your Choice</span>";
    } else {
        return "<span>"+obj.text+"</span>";
    }

}

function onload() {
    let hash = document.location.hash;
    document.getElementById("permlink").href = document.URL;
    document.getElementById("permlink").innerHTML = document.URL;
    if (hash) {
        let arr = atob(hash.substring(1)).split(":");
        if (arr.length == 5) {
            let [a, b, c, d, e] = arr;
            let coffee_water_ratio = COFFEE_WATER_RADIO[a];
            let temp = TEMP[b];
            let grind_brew_time = GRIND_BREW_TIME[c];
            let method = METHOD[d];
            let agitation = AGITATION[e];

            if (coffee_water_ratio && temp && grind_brew_time && method && agitation) {

                document.getElementById("coffee_water_ratio").innerHTML = generate_coffee_water_html(coffee_water_ratio);
                document.getElementById("temp").innerHTML = generate_temp_html(temp);
                document.getElementById("grind_brew_time").innerHTML = generate_gbt_html(grind_brew_time);
                document.getElementById("method").innerHTML = generate_method_html(method);
                document.getElementById("agitation").innerHTML = generate_agitation_html(agitation);
                return;
            }

        }
    }
    generate_recipe();
}

function generate_recipe() {
    let [a, coffee_water_ratio] = random_item(COFFEE_WATER_RADIO);
    let [b, temp] = random_item(TEMP);
    let [c, grind_brew_time] = random_item(GRIND_BREW_TIME);
    let [d, method] = random_item(METHOD);
    let [e, agitation] = random_item(AGITATION);
    document.location.hash = btoa([a,b,c,d,e].join(":"));
    document.getElementById("permlink").href = document.URL;
    document.getElementById("permlink").innerHTML = document.URL;

    document.getElementById("coffee_water_ratio").innerHTML = generate_coffee_water_html(coffee_water_ratio);
    document.getElementById("temp").innerHTML = generate_temp_html(temp);
    document.getElementById("grind_brew_time").innerHTML = generate_gbt_html(grind_brew_time);
    document.getElementById("method").innerHTML = generate_method_html(method);
    document.getElementById("agitation").innerHTML = generate_agitation_html(agitation);
}
